package br.com.eafigueira.todolist.service;

import br.com.eafigueira.todolist.exception.BusinessException;
import br.com.eafigueira.todolist.model.Todo;
import br.com.eafigueira.todolist.repository.TodoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author emerson
 */
@Service
public class TodoService {

    @Autowired
    private TodoRepository todoRepository;

    public Todo save(Todo todo) {
        Todo newTodo;
        if (todo.getId() == null || todo.getId() <= 0) {
            newTodo = new Todo();
        } else {
            newTodo = todoRepository.findOne(todo.getId());
        }
        newTodo.setCompleted(todo.isCompleted());
        newTodo.setDescription(todo.getDescription());
        newTodo.setTitle(todo.getTitle());
        return todoRepository.save(newTodo);
    }

    public List<Todo> findAll(boolean onlyActive) {
        if (onlyActive) {
            return todoRepository.findByCompletedTrue();
        }
        return todoRepository.findAll();
    }

    public Todo update(Todo todoUpdate) {
        Todo todo = todoRepository.findOne(todoUpdate.getId());
        if (todo == null) {
            throw new BusinessException("Todo not found");
        }
        todo.setCompleted(todoUpdate.isCompleted());
        todo.setDescription(todoUpdate.getDescription());
        todo.setTitle(todoUpdate.getTitle());
        return todoRepository.save(todo);
    }

    public void remove(long id) {
        todoRepository.delete(id);
    }
}
