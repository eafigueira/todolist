package br.com.eafigueira.todolist.response;

import org.springframework.http.ResponseEntity;

/**
 *
 * @author emerson
 */
public class RestResponse {

    private RestResponse() { }
    
    public static ResponseEntity<IApiResponse> success() {
        return ResponseEntity.ok(new RestSuccessResponse());
    }
    
    public static ResponseEntity<IApiResponse> success(Object data) {
        return ResponseEntity.ok(new RestSuccessResponse(data));
    }

    public static ResponseEntity<IApiResponse> error(String error) {
        return ResponseEntity.ok(new RestErrorResponse(new String[]{error}));
    }

    public static ResponseEntity<IApiResponse> error(String[] errors) {
        return ResponseEntity.ok(new RestErrorResponse(errors));
    }
    
}
