package br.com.eafigueira.todolist.response;

import com.google.gson.Gson;

/**
 *
 * @author emerson
 */
public interface IApiResponse {
    
    default String toJson() {
        return new Gson().toJson(this);
    }
    
    Object getData();
    
    boolean isSuccess();
    
}
