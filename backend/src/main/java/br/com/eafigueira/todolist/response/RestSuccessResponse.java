package br.com.eafigueira.todolist.response;

import lombok.Data;

/**
 *
 * @author emerson
 */
@Data
public class RestSuccessResponse implements IApiResponse {

    private final Object data;
    private final boolean success = true;

    public RestSuccessResponse() {
        this(new Object());
    }

    public RestSuccessResponse(Object data) {
        if (data == null) {
            this.data = new Object();
        } else {
            this.data = data;
        }
    }

    @Override
    public boolean isSuccess() {
        return success;
    }

}
