package br.com.eafigueira.todolist.exception;

import br.com.eafigueira.todolist.response.RestErrorResponse;
import br.com.eafigueira.todolist.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Classe para interceptar os erros e e formatar a resposta no formato da api
 * @author emerson
 */

@ControllerAdvice
public class ApiResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messages;

    @ExceptionHandler({
        EmptyResultDataAccessException.class,
        DataIntegrityViolationException.class,
        InvalidDataAccessApiUsageException.class,
        BusinessException.class,
        Exception.class
    })
    public final ResponseEntity<Object> handleExceptions(Exception ex, WebRequest request) {
        if (ex instanceof EmptyResultDataAccessException) {
            EmptyResultDataAccessException exception = (EmptyResultDataAccessException) ex;
            return handleEmptyResultDataAccessException(exception, request);

        } else if (ex instanceof DataIntegrityViolationException) {
            DataIntegrityViolationException exception = (DataIntegrityViolationException) ex;
            return handleDataIntegrityViolationException(exception, request);

        } else if (ex instanceof InvalidDataAccessApiUsageException) {
            InvalidDataAccessApiUsageException exception = (InvalidDataAccessApiUsageException) ex;
            return handleInvalidDataAcessApiUsageException(exception, request);

        } else if (ex instanceof BusinessException) {
            BusinessException exception = (BusinessException) ex;
            return handleBusinessException(exception, request);

        } else {
            return handleUnknownException(ex, request);
        }
    }

    @Override
    @SuppressWarnings("ThrowableResultIgnored")
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String body = ex.getCause() != null ? ex.getCause().toString() : ex.toString();
        return handleExceptionInternal(ex, new RestErrorResponse(body), headers, HttpStatus.OK, request);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String body = ExceptionUtils.getRootCauseMessage(ex);
        return handleExceptionInternal(ex, new RestErrorResponse(body), new HttpHeaders(), HttpStatus.OK, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> body = createListErrors(ex.getBindingResult(), request.getLocale());
        return handleExceptionInternal(ex, new RestErrorResponse(body), headers, HttpStatus.OK, request);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (body == null) {
            body = new RestErrorResponse(ex.getMessage());
        }
        return super.handleExceptionInternal(ex, body, headers, HttpStatus.OK, request);
    }

    public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request) {
        String body = messages.getMessage("request.resource-not-found", null, request.getLocale());
        logger.error(ex.toString());
        return handleExceptionInternal(ex, new RestErrorResponse(body), new HttpHeaders(), HttpStatus.OK, request);
    }

    public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request) {
        return handleExceptionInternal(ex, new RestErrorResponse(ExceptionUtils.getRootCauseMessage(ex)), new HttpHeaders(), HttpStatus.OK, request);
    }

    public ResponseEntity<Object> handleInvalidDataAcessApiUsageException(InvalidDataAccessApiUsageException ex, WebRequest request) {
        return handleExceptionInternal(ex, new RestErrorResponse(ExceptionUtils.getRootCauseMessage(ex)), new HttpHeaders(), HttpStatus.OK, request);
    }

    private ResponseEntity<Object> handleBusinessException(BusinessException ex, WebRequest request) {
        String errorMessage;
        try {
            errorMessage = messages.getMessage(ex.getErrorCode(), ex.getArgs(), request.getLocale());
        } catch (NoSuchMessageException e) {
            errorMessage = ex.getMessage();
        }
        return handleExceptionInternal(ex, new RestErrorResponse(errorMessage), new HttpHeaders(), HttpStatus.OK, request);
    }

    private ResponseEntity<Object> handleUnknownException(Exception ex, WebRequest request) {
        String message = ex.getMessage();
        if (StringUtil.isBlank(message) && ex.getCause() != null) {
            message = ex.getCause().getMessage();
        }
        if (StringUtil.isBlank(message)) {
            message = ExceptionUtils.getRootCauseMessage(ex);
        }
        if (StringUtil.isBlank(message)) {
            message = messages.getMessage("exceptions.unknown", null, request.getLocale());
        }
   
        return handleExceptionInternal(ex, new RestErrorResponse(message), new HttpHeaders(), HttpStatus.OK, request);
    }

    private List<String> createListErrors(BindingResult bindingResult, Locale locale) {
        List<String> errors = new ArrayList<>();
        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            errors.add(messages.getMessage(fieldError, locale));
        }
        return errors;
    }    
}
