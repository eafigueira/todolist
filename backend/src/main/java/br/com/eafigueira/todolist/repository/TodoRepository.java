package br.com.eafigueira.todolist.repository;

import br.com.eafigueira.todolist.model.Todo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author emerson
 */
public interface TodoRepository extends JpaRepository<Todo, Long> {
    List<Todo> findByCompletedTrue();
}
