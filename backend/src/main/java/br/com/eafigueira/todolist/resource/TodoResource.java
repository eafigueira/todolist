package br.com.eafigueira.todolist.resource;

import br.com.eafigueira.todolist.model.Todo;
import br.com.eafigueira.todolist.response.IApiResponse;
import br.com.eafigueira.todolist.response.RestResponse;
import br.com.eafigueira.todolist.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author emerson
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/todo")
public class TodoResource {
    
    @Autowired
    private TodoService todoService;

    @PostMapping()
    public ResponseEntity<IApiResponse> save(@RequestBody Todo todo) {
        return RestResponse.success(todoService.save(todo));
    }

    @GetMapping()
    public ResponseEntity<IApiResponse> findAll(@RequestParam(name = "onlyCompleted", defaultValue = "false") boolean onlyCompleted) {
        return RestResponse.success(todoService.findAll(onlyCompleted));
    }

    @PutMapping()
    public ResponseEntity<IApiResponse> update(@RequestBody Todo todo) {
        return RestResponse.success(todoService.update(todo));
    }

    @DeleteMapping()
    public ResponseEntity<IApiResponse> remove(@RequestParam("id") long id) {
        todoService.remove(id);
        return RestResponse.success();
    }

}
