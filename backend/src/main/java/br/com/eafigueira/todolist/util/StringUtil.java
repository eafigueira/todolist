package br.com.eafigueira.todolist.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

/**
 *
 * @author emerson
 */
public class StringUtil {
    public static boolean isBlank(Object obj) {
        if (obj == null) {
            return true;
        }
        if (obj.getClass().isArray()) {
            return Array.getLength(obj) == 0;
        }
        if (obj instanceof Collection) {
            return ((Collection<?>) obj).isEmpty();
        }
        if (obj instanceof Map) {
            return ((Map<?, ?>) obj).isEmpty();
        }
        String str = String.valueOf(obj);
        return str.isEmpty();
    }
    
}
