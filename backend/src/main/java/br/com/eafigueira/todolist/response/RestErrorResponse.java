package br.com.eafigueira.todolist.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import lombok.Data;

/**
 *
 * @author emerson
 */
@Data
public class RestErrorResponse implements IApiResponse {

    private final String[] errors;
    private final boolean success = false;

    public RestErrorResponse(String error) {
        this(new String[]{error});
    }

    public RestErrorResponse(String[] errors) {
        this.errors = errors;
    }

    public RestErrorResponse(List<String> errors) {
        if (errors == null || errors.isEmpty()) {
            this.errors = new String[0];
        } else {
            this.errors = errors.toArray(new String[errors.size()]);
        }
    }

    public String[] getErrors() {
        return errors;
    }

    @JsonIgnore
    @Override
    public Object getData() {
        return null;
    }

    @Override
    public boolean isSuccess() {
        return success;
    }

}
