import styled, { css } from "styled-components";

export const TodoListTitle = styled.h1`
  font-size: 2rem;
  font-weight: 700;
  padding: 0 1.4rem;
  margin: 1rem auto;
`;

export const CenterLayout = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const TodoContainer = styled.div``;

export const TodoListContainer = styled.div`
  max-height: 280px;
  min-height: 280px;
  overflow-x: auto;
`;

export const TextInput = styled.input`
  text-size-adjust: 100%;
  box-sizing: inherit;
  margin: 0;
  align-items: center;
  border: 1px solid transparent;
  display: inline-flex;
  font-size: 1rem;
  height: 2.5em;
  justify-content: flex-start;
  line-height: 1.5;
  background-color: #fff;
  border-radius: 4px;
  color: inherit;
  box-shadow: inset 0 0.0625em 0.125em rgba(10, 10, 10, 0.05);
  max-width: 100%;
  width: 100%;
  border-color: #00d1b2;
`;

export const TextArea = styled.textarea`
  text-size-adjust: 100%;
  box-sizing: inherit;
  margin: 0;
  align-items: center;
  border: 1px solid transparent;
  font-size: 1rem;
  height: 2.5em;
  justify-content: flex-start;
  line-height: 1.5;
  background-color: #fff;
  border-radius: 4px;
  color: inherit;
  box-shadow: inset 0 0.0625em 0.125em rgba(10, 10, 10, 0.05);
  width: 100%;
  display: block;
  max-width: 100%;
  min-width: 100%;
  resize: vertical;
  border-color: #00d1b2;
  max-height: 40em;
  min-height: 8em;
`;

export const Checkbox = styled.input`
  text-size-adjust: 100%;
  box-sizing: inherit;
  margin: 0;
  vertical-align: baseline;
  cursor: pointer;
`;

export const Field = styled.div`
  text-rendering: optimizeLegibility;
  text-size-adjust: 100%;
  color: #4a4a4a;
  font-size: 1em;
  font-weight: 400;
  line-height: 1.5;
  box-sizing: inherit;
  margin-bottom: 0.75rem;
`;
export const Label = styled.label`
  text-rendering: optimizeLegibility;
  text-size-adjust: 100%;
  line-height: 1.5;
  box-sizing: inherit;
  color: #363636;
  display: block;
  font-size: 1rem;
  font-weight: 700;
  margin-bottom: 0.5em;
`;

export const Button = styled.button`
  text-size-adjust: 100%;
  box-sizing: inherit;
  margin: 0;
  user-select: none;
  align-items: center;
  border: 1px solid transparent;
  border-radius: 4px;
  box-shadow: none;
  display: inline-flex;
  font-size: 1rem;
  height: 2.5em;
  line-height: 1.5;
  position: relative;
  vertical-align: top;
  border-width: 1px;
  cursor: pointer;
  justify-content: center;
  text-align: center;
  white-space: nowrap;
  background-color: #00d1b2;
  border-color: transparent;
  color: #fff;
  min-width: ${props => (props.fullWidth ? "100%" : "0")};
`;

export const DeleteTodo = styled.button`
  margin-top: 5px;
  text-size-adjust: 100%;
  box-sizing: inherit;
  user-select: none;
  align-items: center;
  border: 1px solid transparent;
  border-radius: 4px;
  box-shadow: none;
  display: inline-flex;
  font-size: 1rem;
  height: 2.5em;
  line-height: 1.5;
  position: relative;
  vertical-align: top;
  border-width: 1px;
  cursor: pointer;
  justify-content: center;
  text-align: center;
  white-space: nowrap;
  background-color: #f14668;
  border-color: transparent;
  color: #fff;
  min-width: ${props => (props.fullWidth ? "100%" : "0")};
`;

export const List = styled.div`
  margin-top: 10px;
  text-rendering: optimizeLegibility;
  text-size-adjust: 100%;
  color: #00d1b2;
  font-size: 1em;
  font-weight: 400;
  line-height: 1.5;
  box-sizing: inherit;
  background-color: #fff;
  border-radius: 4px;
  box-shadow: 0 2px 3px rgba(10, 10, 10, 0.1), 0 0 0 1px rgba(10, 10, 10, 0.1);
  width: 100%;
`;

export const Item = styled.div`
  text-rendering: optimizeLegibility;
  text-size-adjust: 100%;
  font-size: 1em;
  font-weight: 400;
  line-height: 1.5;
  box-sizing: inherit;
  color: #00d1b2;
  text-decoration: none;
  display: block;
  padding: 0.5em 1em;
  background-color: #f5f5f5;
  cursor: pointer;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  border-bottom: 1px solid #dbdbdb;
  text-decoration: ${props => (props.completed ? "line-through" : "")};
`;
