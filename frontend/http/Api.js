import Axios from "axios";

export function Api() {
  const baseURL = "http://localhost:4000/api/v1/todo/";
  return {
    updateTodo: todo => {
      return new Promise((resolve, reject) => {
        Axios.put(baseURL, todo)
          .then(response => {
            if (response.data.success) {
              resolve(response.data.data);
            } else {
              reject(response.data.errors);
            }
          })
          .catch(reject);
      });
    },
    insertTodo: todo => {
      return new Promise((resolve, reject) => {
        const { id, ...rest } = todo;
        Axios.post(baseURL, rest)
          .then(response => {
            if (response.data.success) {
              resolve(response.data.data);
            } else {
              reject(response.data.errors);
            }
          })
          .catch(reject);
      });
    },
    removeTodo: id => {
      return new Promise((resolve, reject) => {
        Axios.delete(`${baseURL}?id=${id}`)
          .then(response => {
            if (response.data.success) {
              resolve(response.data.data);
            } else {
              reject(response.data.errors);
            }
          })
          .catch(reject);
      });
    },
    listAllTodos: onlyCompleted => {
      return new Promise((resolve, reject) => {
        let url;
        if (onlyCompleted) url = `${baseURL}?onlyCompleted=true`;
        else url = `${baseURL}`;
        Axios.get(url)
          .then(response => {
            if (response.data.success) {
              resolve(response.data.data);
            } else {
              reject(response.data.errors);
            }
          })
          .catch(reject);
      });
    }
  };
}
