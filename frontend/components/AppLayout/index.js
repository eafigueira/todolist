import { CenterLayout } from "../../styles/Base";

export const AppLayout = ({ children }) => {
  return <CenterLayout>{children}</CenterLayout>;
};
