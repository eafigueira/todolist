import React, { useState } from "react";
import { Item } from "../../styles/Base";

export function ListItem(props) {
  const { title, completed } = props.todo;
  return (
    <Item completed={completed} onClick={props.onClick}>
      {title}
    </Item>
  );
}
