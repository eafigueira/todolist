import React, { useState, useEffect, Fragment } from "react";
import { Form } from "./Form";
import { ListItem } from "./ListItem";
import { List } from "../../styles/Base";
import { Api } from "../../http/Api";

export function TodoApp() {
  const [todos, setTodos] = useState([]);
  const [loading, setlLoading] = useState(true);
  const [selectedTodo, setSelectedTodo] = useState(undefined);

  const api = Api();

  useEffect(() => {
    api.listAllTodos(false).then(response => {
      setTodos(response);
      setlLoading(false);
    });
  }, []);

  const handleSubmit = todo => {
    if (todo.id > 0) {
      setlLoading(true);
      api.updateTodo(todo).then(() => {
        api.listAllTodos(false).then(response => {
          setTodos(response);
          setlLoading(false);
          setSelectedTodo(undefined);
        });
      });
    } else {
      setlLoading(true);
      api.insertTodo(todo).then(() => {
        api.listAllTodos(false).then(response => {
          setTodos(response);
          setlLoading(false);
          setSelectedTodo(undefined);
        });
      });
    }
  };

  const handleRemove = id => {
    setlLoading(true);
    api.removeTodo(id).then(() => {
      api.listAllTodos(false).then(response => {
        setTodos(response);
        setlLoading(false);
        setSelectedTodo(undefined);
      });
    });
  };

  return (
    <Fragment>
      <Form
        onSubmit={handleSubmit}
        selectedTodo={selectedTodo}
        onRemove={handleRemove}
        loading={loading}
      />
      <List>
        {todos.map((todo, index) => (
          <ListItem
            key={index}
            todo={todo}
            onClick={() => {
              setSelectedTodo(todo);
            }}
          />
        ))}
      </List>
    </Fragment>
  );
}
