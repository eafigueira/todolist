import React, { useRef, useEffect } from "react";
import {
  TextInput,
  TextArea,
  Checkbox,
  Field,
  Label,
  Button,
  DeleteTodo
} from "../../styles/Base";

export function Form(props) {
  const inputTitleRef = useRef();
  const inputDescriptionRef = useRef();
  const inputCompletedRef = useRef();

  useEffect(() => {
    inputTitleRef.current.value = props.selectedTodo
      ? props.selectedTodo.title
      : "";
    inputDescriptionRef.current.value = props.selectedTodo
      ? props.selectedTodo.description
      : "";
    inputCompletedRef.current.checked = props.selectedTodo
      ? props.selectedTodo.completed
      : false;
  }, [props.selectedTodo]);

  const handleSubmit = event => {
    event.preventDefault();

    const title = inputTitleRef.current.value;
    const description = inputDescriptionRef.current.value;
    const completed = inputCompletedRef.current.checked;

    const todo = {
      id: props.selectedTodo ? props.selectedTodo.id : 0,
      title,
      description,
      completed
    };

    props.onSubmit(todo);

    inputTitleRef.current.value = "";
    inputDescriptionRef.current.value = "";
    inputCompletedRef.current.checked = false;
  };

  return (
    <form onSubmit={handleSubmit}>
      <Field>
        <Label>Title</Label>
        <TextInput type="text" ref={inputTitleRef} required />
      </Field>
      <Field>
        <Label>Description</Label>
        <TextArea type="text" ref={inputDescriptionRef} />
      </Field>
      <Field>
        <Label>Completed?</Label>
        <Checkbox type="checkbox" ref={inputCompletedRef} />
      </Field>
      <Button type="submit" fullWidth disabled={props.loading}>
        {props.selectedTodo ? "Update todo" : "New todo"}
      </Button>
      {props.selectedTodo && (
        <DeleteTodo
          fullWidth
          disabled={props.loading}
          onClick={() => props.onRemove(props.selectedTodo.id)}
        >
          Delete todo
        </DeleteTodo>
      )}
    </form>
  );
}
