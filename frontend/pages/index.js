import Head from "next/head";
import React, { Fragment } from "react";
import { AppLayout } from "../components/AppLayout";
import { TodoApp } from "../components/Todo/TodoApp";
import { TodoContainer } from "../styles/Base";

const Home = () => {
  return (
    <Fragment>
      <Head>
        <title>TODO LIST</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <AppLayout>
        <TodoContainer>
          <TodoApp />
        </TodoContainer>
      </AppLayout>
    </Fragment>
  );
};

export default Home;
